#!/bin/sh -

# until we get proper build tools and the awaited repkg replacement/rewrite,
# here's some intermediate hack

set -uf

die()
{
	{
		# shellcheck disable=SC2059
		printf "$@"
		printf '\n'
	} >&2
}

# Get the package list:
packages=$(diffrepo -sb "$CHECKUPDATES_DB" -R -n base-devel -n makepkg -n urxvt-url-select zuepfe-repkg 2>&1)
retval=$?
if [ $retval -eq 0 ]; then
	printf 'No packages to rebuild.\n'
	exit 0
fi
if [ $retval -ne 6 ]; then
	printf 'Could not get packages to be rebuilt: %s\n' "$packages" >&2
	exit 1
fi
if [ -z "$packages" ]; then
	printf 'Obtained empty list of packages to be rebuilt!\n' >&2
	exit 1
fi
printf 'Rebuilding the following packages:\n%s\n' "$packages"

rmkpkg()
{
	package=$1
	printf 'Rebuilding \033[1m%s\033[0m...\n' "$package"
	if [ ! -d "$package" ]; then
		(cd _master && git fetch -p && git rebase) \
			&& cp -rv _master "$package" \
			&& (cd "$package" && git checkout "$package")
	fi
	(cd "$package" && git fetch -p && git rebase && remakepkg -b "$CHECKUPDATES_DB") || exit 1
}

# Remake the packages, and also assemble commandline
set --
have_pacman=false
while read -r _ package; do
	package=${package#*/}
	rmkpkg "$package"
	set +f
	set -- "$package"/"$package"-*.pkg.tar.zst "$@"
	set -f
	test -f "$1" || die 'Failed to match package file: %s' "$1"
	if [ "$package" = 'pacman' ]; then
		have_pacman=true
	fi
done << EOF
$packages
EOF

if $have_pacman; then
	printf 'Pacman rebuilt, so adding makepkg\n'
	rmkpkg makepkg
	set +f
	set -- makepkg/makepkg-*.pkg.tar.zst "$@"
	set -f
	test -f "$1" || die 'Failed to match package file: %s' "$1"
fi

printf 'Adding the following files:\n%s\n' "$*"
zr add -f "$@"
rm -v "$@"

printf 'Refreshing package DB\n'
checkupdates
