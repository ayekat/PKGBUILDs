PKGBUILDs
=========

This repository contains my personal PKGBUILDs, METAPKGBUILDs and REPKGBUILDs
for Arch Linux.

Builds can be found in the [zuepfe repositories][rp].


[zuepfe-original]
-----------------

These are all regular packages built from (self-maintained) PKGBUILD files, and
most builds can be found in the [[zuepfe-original]][rp:zo] repository.

| Name | Comment |
| --- | --- |
| [`ayekat-utils-git`][b:ayekat-utils-git] | &mdash; |
| [`archlinux-mirror`][b:archlinux-mirror] | replacement of full-blown (and constantly changing) `pacman-mirrorlist` |
| [`bl-git`][b:bl-git]                     | &mdash; |
| [`ca-certificates-zuepfe`][b:ca-certificates-zuepfe] | &mdash; |
| [`cucumber-git`][b:cucumber-git]         | &mdash; |
| [`dirwatchdog-git`][b:dirwatchdog-git]   | &mdash; |
| [`fanctl-git`][b:fanctl-git]             | &mdash; |
| [`karuibar-git`][b:karuibar-git]         | karuibar + all modules |
| [`karuiwm-git`][b:karuiwm-git]           | karuiwm, with my personal configuration |
| [`keymaps-ayekat`][b:keymaps-ayekat]     | &mdash; |
| [`open-git`][b:open-git]                 | &mdash; |
| [`otf-kouzan-gyousho`][b:otf-kouzan-gyousho] | *Kouzan Gyousho* OTF font |
| [`pacman-hacks`/`pacman-hacks-build`][b:pacman-hacks] | &mdash; |
| [`pacman-hacks-git`/`pacman-hacks-build-git`][b:pacman-hacks-git] | VCS for `pacman-hacks` *(not in [zuepfe-original])* |
| [`pastebins-git`][b:pastebins-git]       | &mdash; |
| [`pooch-git`][b:pooch-git]               | &mdash; |
| [`qbsmgr-git`][b:qbsmgr-git]             | &mdash; |
| [`spindown-heidubeeri`][b:spindown]      | originally [`AUR/spindown`][ap:spindown], with a patch to work on my home server (see also #9) |
| [`system-config`][b:system-config]       | system configuration files, packaged |
| [`totp-git`][b:totp-git]                 | &mdash; |
| [`user`][b:user]                         | create/manage (some aspects of) user accounts |
| [`vmctl-git`][b:vmctl-git]               | &mdash; |
| [`xor-git`][b:xor-git]                   | &mdash; |
| [`zuepfe-keyring`][b:zuepfe-keyring]     | &mdash; |


[zuepfe-meta]
-------------

These are all meta-packages built from [METAPKGBUILD][man:makemetapkg] files,
and the builds can be found in the [[zuepfe-meta]][rp:zm] repository.

| Name | Comment |
| --- | --- |
| [`devel`][b:devel]                 | Development |
| [`epfl`][b:epfl]                   | EPFL |
| [`services`][b:services]           | Online services |
| [`zuepfe`][b:zuepfe]               | Personal system setups |


[zuepfe-repkg]
--------------

These are mostly repackaged packages built from [REPKGBUILD][man:remakepkg]
files, and the builds can be found in the [[zuepfe-repkg]][rp:zr] repository.

| Name | Comment |
| --- | --- |
| [`acpid`][b:acpid]             | Move `anything` rule to doc/examples |
| [`bash`][b:bash]               | Remove `/bin/sh` ([FS#14086][fs:14086], [FS#38766][fs:38766], [FS#42134][fs:42134]); move `/etc/skel` files to `/usr/share/doc` |
| [`dash`][b:dash]               | Add `/bin/sh` ([FS#14086][fs:14086], [FS#38766][fs:38766], [FS#42134][fs:42134]) |
| [`debootstrap`][b:debootstrap] | Add missing dependencies `sh` and `pacman` |
| [`dnsmasq`][b:dnsmasq]         | Enable inclusion of `.conf` files from `/etc/dnsmasq.d` by default; Add instanced dnsmasq service units (also from `/etc/dnsmasq.d`) |
| [`gdm`][b:gdm]                 | Extend `/etc/gdm/Xsession` to respect XDG basedir spec |
| [`glibc`][b:glibc]             | Add missing optional dependency `sed` for `locale-gen` ([FS#55635][fs:55635]); patch the horror out of `locale-gen` |
| [`grub`][b:grub]               | Add missing `coreutils` dependency for install script |
| [`gvim`][b:gvim]               | &mdash; (only here due to versioned dependency on repackaged `vim-runtime`) |
| [`hostapd`][b:hostapd]         | Move `/etc/hostapd` files into `/usr/share/doc` |
| [`makepkg`][b:makepkg]         | (new package) makepkg split from the pacman package |
| [`mercurial`][b:mercurial]     | Remove weird (executable) profile snippet that only exports `HG=/usr/bin/hg` |
| [`ncurses`][b:ncurses]         | Add dependency on `rxvt-unicode-terminfo` (to work around [upstream issues][ncurses:urxvt]) |
| [`ntfs-3g`][b:ntfs-3g]         | Add optional dependency [`ntfs-3g-system-compression`][ap:ntfs-3g-system-compression] for additional NTFS reparse point types support |
| [`pacman`][b:pacman]           | Remove makepkg components (split to a [separate package][b:makepkg]); backport [49ebd856][pacman:49ebd856] |
| [`pacman-contrib`][b:pacman-contrib] | Patch `checkupdates` to not hide all pacman output; add missing optional dependency `diffutils` |
| [`pass`][b:pass]               | Add missing dependency `findutils` |
| [`qemu-common`][b:qemu-common] | Move `/etc/qemu/bridge.conf` to `/usr/share/doc` |
| [`shadow`][b:shadow]           | Add missing `libcap` and `coreutils` dependencies for install script |
| [`strongswan`][b:strongswan]   | Move `/etc/ipsec.{conf,secrets}` files to `/usr/share/doc` |
| [`urxvt-perls`][b:urxvt-perls] | Add provisions for individual perl scripts |
| [`vim`][b:vim]                 | &mdash; (only here due to versioned dependency on repackaged `vim-runtime`) |
| [`vim-runtime`][b:vim-runtime] | Undo downstream patch that force-creates `XDG_CACHE_HOME/vim` |
| [`whois`][b:whois]             | Provide and conflict with `mkpasswd` |
| [`xdg-utils`][b:xdg-utils]     | Add missing dependencies for `xdg-open` (`coreutils`, `grep`, `sed`) |
| [`xorg-xinit`][b:xorg-xinit]   | Add missing dependencies and remove unnecessary dependencies |


Unmaintained
------------

Packages that I stop maintaining undergo the following procedure:

* All files in the package's branch are deleted;
* The branch is merged (without fast-forwarding) into the (empty) branch
  `graveyard`;
* The package branch is deleted.

This allows me to keep the history of packages that are no longer maintained,
and at the same time keep the repository clean.


[ap:ntfs-3g-system-compression]: https://aur.archlinux.org/packages/ntfs-3g-system-compression/
[ap:spindown]: https://aur.archlinux.org/packages/spindown/
[b:acpid]: https://gitlab.com/ayekat/PKGBUILDs/tree/acpid
[b:archlinux-mirror]: https://gitlab.com/ayekat/PKGBUILDs/tree/archlinux-mirror
[b:bash]: https://gitlab.com/ayekat/PKGBUILDs/tree/bash
[b:bl-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/bl-git
[b:ca-certificates-zuepfe]: https://gitlab.com/ayekat/PKGBUILDs/tree/ca-certificates-zuepfe
[b:cucumber-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/cucumber-git
[b:dash]: https://gitlab.com/ayekat/PKGBUILDs/tree/dash
[b:debootstrap]: https://gitlab.com/ayekat/PKGBUILDs/tree/debootstrap
[b:devel]: https://gitlab.com/ayekat/PKGBUILDs/tree/devel
[b:dirwatchdog-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/dirwatchdog-git
[b:dnsmasq]: https://gitlab.com/ayekat/PKGBUILDs/tree/dnsmasq
[b:epfl]: https://gitlab.com/ayekat/PKGBUILDs/tree/epfl
[b:fanctl-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/fanctl-git
[b:gdm]: https://gitlab.com/ayekat/PKGBUILDs/tree/gdm
[b:glibc]: https://gitlab.com/ayekat/PKGBUILDs/tree/glibc
[b:grub]: https://gitlab.com/ayekat/PKGBUILDs/tree/grub
[b:gvim]: https://gitlab.com/ayekat/PKGBUILDs/tree/gvim
[b:hostapd]: https://gitlab.com/ayekat/PKGBUILDs/tree/hostapd
[b:karuibar-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/karuibar-git
[b:keymaps-ayekat]: https://gitlab.com/ayekat/PKGBUILDs/tree/keymaps-ayekat
[b:karuiwm-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/karuiwm-git
[b:makepkg]: https://gitlab.com/ayekat/PKGBUILDs/tree/makepkg
[b:mercurial]: https://gitlab.com/ayekat/PKGBUILDs/tree/mercurial
[b:ncurses]: https://gitlab.com/ayekat/PKGBUILDs/tree/ncurses
[b:ntfs-3g]: https://gitlab.com/ayekat/PKGBUILDs/tree/ntfs-3g
[b:open-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/open-git
[b:otf-kouzan-gyousho]: https://gitlab.com/ayekat/PKGBUILDs/tree/otf-kouzan-gyousho
[b:pacman]: https://gitlab.com/ayekat/PKGBUILDs/tree/pacman
[b:pacman-contrib]: https://gitlab.com/ayekat/PKGBUILDs/tree/pacman-contrib
[b:pacman-hacks]: https://gitlab.com/ayekat/PKGBUILDs/tree/pacman-hacks
[b:pacman-hacks-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/pacman-hacks-git
[b:pass]: https://gitlab.com/ayekat/PKGBUILDs/tree/pass
[b:pastebins-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/pastebins-git
[b:pooch-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/pooch-git
[b:qbsmgr-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/qbsmgr-git
[b:qemu-common]: https://gitlab.com/ayekat/PKGBUILDs/tree/qemu-common
[b:services]: https://gitlab.com/ayekat/PKGBUILDs/tree/services
[b:shadow]: https://gitlab.com/ayekat/PKGBUILDs/tree/shadow
[b:spindown]: https://gitlab.com/ayekat/PKGBUILDs/tree/spindown-heidubeeri
[b:strongswan]: https://gitlab.com/ayekat/PKGBUILDs/tree/strongswan
[b:system-config]: https://gitlab.com/ayekat/PKGBUILDs/tree/system-config
[b:totp-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/totp-git
[b:urxvt-perls]: https://gitlab.com/ayekat/PKGBUILDs/tree/urxvt-perls
[b:user]: https://gitlab.com/ayekat/PKGBUILDs/tree/user
[b:vim]: https://gitlab.com/ayekat/PKGBUILDs/tree/vim
[b:vim-runtime]: https://gitlab.com/ayekat/PKGBUILDs/tree/vim-runtime
[b:vmctl-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/vmctl-git
[b:whois]: https://gitlab.com/ayekat/PKGBUILDs/tree/whois
[b:xdg-utils]: https://gitlab.com/ayekat/PKGBUILDs/tree/xdg-utils
[b:xor-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/xor-git
[b:ayekat-utils-git]: https://gitlab.com/ayekat/PKGBUILDs/tree/ayekat-utils-git
[b:xorg-xinit]: https://gitlab.com/ayekat/PKGBUILDs/tree/xorg-xinit
[b:zuepfe]: https://gitlab.com/ayekat/PKGBUILDs/tree/zuepfe
[b:zuepfe-keyring]: https://gitlab.com/ayekat/PKGBUILDs/tree/zuepfe-keyring
[fs:14086]: https://bugs.archlinux.org/task/14086
[fs:38766]: https://bugs.archlinux.org/task/38766
[fs:42134]: https://bugs.archlinux.org/task/42134
[fs:48908]: https://bugs.archlinux.org/task/48908
[fs:51931]: https://bugs.archlinux.org/task/51931
[fs:55635]: https://bugs.archlinux.org/task/55635
[man:makemetapkg]: https://gitlab.com/ayekat/pacman-hacks/blob/master/doc/makemetapkg.1.asciidoc
[man:remakepkg]: https://gitlab.com/ayekat/pacman-hacks/blob/master/doc/remakepkg.1.asciidoc
[ncurses:urxvt]: http://invisible-island.net/ncurses/ncurses-urxvt.html
[pacman:49ebd856]: https://gitlab.archlinux.org/pacman/pacman/-/commit/49ebd856ec09b2a7d87d36dedbf83d02d59269a8
[rp]: http://archlinux.zuepfe.net/
[rp:zo]: http://archlinux.zuepfe.net/zuepfe-original/os/x86_64/
[rp:zm]: http://archlinux.zuepfe.net/zuepfe-meta/os/x86_64/
[rp:zr]: http://archlinux.zuepfe.net/zuepfe-repkg/os/x86_64/
